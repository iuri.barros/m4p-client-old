FROM nginx:stable-alpine
LABEL maintainer="joserobertovasconcelos@gmail.com"


WORKDIR /usr/share/nginx/html/

ENV VAR__API_URL='https://dv.manatustecnologia.com.br/m4p/api'
ENV VAR__CONTEXT=m4p

COPY dist /opt/${VAR__CONTEXT}

EXPOSE 80
CMD rm -rf /usr/share/nginx/html;mkdir -p "/usr/share/nginx/html/$VAR__CONTEXT";ln -s /opt/$VAR__CONTEXT/* "/usr/share/nginx/html/$VAR__CONTEXT/"; printenv|grep 'VAR__'|while IFS='=' read -r name value ; do grep -rl $name /usr/share/nginx/html/* | xargs sed -i '' -e "s#$name#$value#g" ; done; nginx -g 'daemon off;'
