module.exports = class User {
  constructor() {
    this.id = 0,
    this.user_avatar = '',
    this.user_username = '',
    this.user_email = '',
    this.user_cpf = '',
    this.user_rg = '',
    this.user_password = '',
    this.profile_id = 1,
    this.st_active = '',
    this.st_registry_active = '',
    this.created = '',
    this.modified = ''
  }
}
