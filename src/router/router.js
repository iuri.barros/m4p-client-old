import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('@/views/dashboard/dashboard/dashboard'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Dashboard',
        pageIcon: 'shop',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/modules',
      name: 'modules',
      component: () => import('@/views/dashboard/modules/modules'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Módulos',
        pageIcon: 'radar-chart',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/maturity-assessment',
      name: 'maturity-assessment',
      component: () =>
        import('@/views/dashboard/maturity-assessment/maturity-assessment'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Avaliação de Maturidade',
        pageIcon: 'radar-chart',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/strategic-planning',
      name: 'strategic-planning',
      component: () =>
        import('@/views/dashboard/strategic-planning/strategic-planning'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Planejamento Estratégico',
        pageIcon: 'appstore',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/plan-management',
      name: 'plan-management',
      component: () =>
        import('@/views/dashboard/plan-management/plan-management'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Gestão de Planos',
        pageIcon: 'control',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/registers',
      name: 'registers',
      component: () =>
        import('@/views/dashboard/registers/registers'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Cadastros',
        pageIcon: 'control',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/registers/plans',
      name: 'registers/plans',
      component: () =>
        import('@/views/dashboard/registers/plans/register-plans'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Planos',
        pageIcon: 'control',
        backPage: true,
        sideBar: false
      },
    },
    {
      path: '/projects-management',
      name: 'projects-management',
      component: () =>
        import('@/views/dashboard/projects-management/projects-management'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Gestão de Projetos',
        pageIcon: 'block',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/chest-management',
      name: 'chest-management',
      component: () =>
        import('@/views/dashboard/chest-management/chest-management'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Gestão de BAU',
        pageIcon: 'apartment',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/program-management',
      name: 'program-management',
      component: () =>
        import('@/views/dashboard/program-management/program-management'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Gestão de Programas',
        pageIcon: 'sliders',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/portfolio-assessment',
      name: 'portfolio-assessment',
      component: () =>
        import('@/views/dashboard/portfolio-assessment/portfolio-assessment'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Gestão de Portfólios',
        pageIcon: 'heat-map',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/risk-management',
      name: 'risk-management',
      component: () =>
        import('@/views/dashboard/risk-management/risk-management'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Gestão de Riscos',
        pageIcon: 'stock',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/processes-management',
      name: 'processes-management',
      component: () =>
        import('@/views/dashboard/processes-management/processes-management'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Gestão de Processos',
        pageIcon: 'fund',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/users',
      name: 'users',
      component: () => import('@/views/dashboard/users/users'),
      meta: {
        guest: true,
        layout: 'layout-dashboard',
        pageName: 'Usuários',
        pageIcon: 'team',
        backPage: false,
        sideBar: true
      },
    },
    {
      path: '/authentication/login',
      name: 'authentication-login',
      component: () => import('@/views/fullscreen/authentication/login'),
      meta: {
        guest: true,
        layout: 'layout-fullscreen',
        pageName: 'Login',
        pageIcon: 'lock',
        backPage: false,
        sideBar: false
      },
    },
    {
      path: '/',
      redirect: '/authentication/login',
      component: () => import('@/views/fullscreen/authentication/login'),
      meta: {
        guest: false,
        layout: 'layout-fullscreen',
        pageName: 'Login',
        pageIcon: 'lock',
        backPage: false,
        sideBar: false
      },
    },
  ],
})

export default router
