import axios from 'axios'

const apiRequest = axios.create({
  baseURL: process.env.VUE_APP_API,
})

let api = {
  /**
   * ###############################################
   * ### USERS
   * ###############################################
   */
  getUsers: () => {
    return apiRequest.get('users')
  },
  getTop10Users: () => {
    return apiRequest.get('users/top-10-users')
  },

  postUsers: (data) => {
    return apiRequest.post('users', data)
  },
}
export default api
