import Vue from 'vue'
import Antd from 'ant-design-vue'
import router from './router/router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import App from './App.vue'
import 'ant-design-vue/dist/antd.css'
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(Antd)

// global style
require('../src/assets/css/style.css')

// Layouts
import Dashboard from './components/layouts/dashboard/dashboard'
import Fullscreen from './components/layouts/fullscreen/fullscreen'
Vue.component('layout-dashboard', Dashboard)
Vue.component('layout-fullscreen', Fullscreen)

Vue.config.productionTip = false

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app')
