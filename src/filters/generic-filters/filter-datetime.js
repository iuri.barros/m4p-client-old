/**
 * Formatar status dos pedidos em string
 */
import Vue from 'vue'
import moment from 'moment'
Vue.filter('filterDateTime', function(dateTime) {
  return moment(dateTime).format('DD/MM/YYYY hh:mm:ss')
})
