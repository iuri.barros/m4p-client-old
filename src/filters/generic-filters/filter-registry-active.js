/**
 * Formatar status dos pedidos em string
 */
import Vue from 'vue'
Vue.filter('filter-registry-active', function(value) {
  let status = ''
  if (value === 'S') {
    status = 'Ativo'
  } else if (value === 'N') {
    status = 'Inativo'
  }
  return status
})
